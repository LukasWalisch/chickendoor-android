import { Frame } from '@nativescript/core/ui/frame/frame';
import { Observable } from '@nativescript/core/data/observable';
import { getBluetoothInstance, Bluetooth, DiscoverOptions } from 'nativescript-bluetooth';
import { ObservableArray } from '@nativescript/core/data/observable-array/observable-array';
import { ItemEventData } from '@nativescript/core/ui/list-view';
import * as Toast from 'nativescript-toast';
import { HelloWorldModel } from './main-view-model';
import { TextDecoder, TextEncoder } from 'text-encoding';


export class ConnectPageModel extends Observable {
    public msg: string;
    public devices: ObservableArray<any>;
    public debugDevices: ObservableArray<any>;
    private bluetooth: Bluetooth;

    private readonly SERVICE_UUID: string = '631be1aa-1e60-4bca-85cb-6c2768a49124';
    private readonly OPEN_CHARACTERISTIC_UUID = '14762daf-c9c6-40dd-8f3b-a3b9aa1fbbdf';
    private readonly CLOSE_CHARACTERISTIC_UUID = 'f305f47c-8aba-4228-93e0-b173b63a05c4';
    private readonly TIMESYNC_CHARACTERISTIC_UUID = 'fc1c00e3-e15c-4a3f-9f89-3ef9363b815c';

    constructor() {
        super();
        this.msg = 'connect page';
        this.devices = new ObservableArray();
        this.debugDevices = new ObservableArray();
        this.bluetooth = getBluetoothInstance();
    }

    async test() {
        console.log("test");
    }

    async scanDevices(): Promise<void> {

        // scan can only be performed if bluetooth is enabled
        if (!this.bluetooth.isBluetoothEnabled()) {
            const toast = Toast.makeText('Bluetooth is not available for this devices');
            toast.show();
            return;
        }

        try {
            await this.bluetooth.startScanning({
                seconds: 4,
                onDiscovered: (peripheral) => {
                    let isDuplicate = false;
                    this.devices.forEach((device) => {
                        if (device.deviceId === peripheral.UUID) {
                            isDuplicate = true;
                        }
                    });
                    if (!isDuplicate && peripheral.name !== null) {
                        this.devices.push({
                            deviceId: peripheral.UUID,
                            deviceName: peripheral.name,
                        });
                    }
                    this.debugDevices.push({
                        deviceId: peripheral.UUID,
                        deviceName: peripheral.name,
                    });
                },
                skipPermissionCheck: false,
            });
        } catch (err) {
            Toast.makeText(`error while scanning: ${err}`).show();
            console.log(`error while scanning: ${err}`);
        }
        Toast.makeText('scanning complete').show();
        console.log('scanning complete');
    }

    onItemTap(args: ItemEventData) {
        const uuid = this.devices.getItem(args.index).deviceId;
        this.connectDevice(uuid);
    }

    async connectDevice(uuid: string) {
        this.bluetooth.connect({
            UUID: uuid,
            onConnected: async (peripheral) => {
                Toast.makeText('connected!').show();
                const today = new Date();
                const msg = `${today.getHours()};${today.getMinutes()};${today.getSeconds()}`;
                const bytes = await new TextEncoder('utf-8').encode(msg);
                this.bluetooth.write({
                    peripheralUUID: peripheral.UUID,
                    serviceUUID: this.SERVICE_UUID,
                    characteristicUUID: this.TIMESYNC_CHARACTERISTIC_UUID,
                    value: bytes,
                });
                Toast.makeText('Clock of ESP32 synchronized');

                const frame = Frame.getFrameById('root-frame');
                frame.navigate({
                    moduleName: 'main-page',
                    bindingContext: new HelloWorldModel(this.bluetooth, peripheral.UUID),
                });
            },
        });
    }
}
