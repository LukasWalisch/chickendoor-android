import { Observable, EventData } from 'tns-core-modules/data/observable';
import { Bluetooth } from 'nativescript-bluetooth';
import { TextDecoder, TextEncoder } from 'text-encoding';
import * as Toast from 'nativescript-toast';
import { TimePicker } from 'tns-core-modules/ui/time-picker';
import { isAndroid } from '@nativescript/core/ui/page/page';

declare let java: any;

export class HelloWorldModel extends Observable {
    public openHour: number;
    public openMinute: number;
    public closeHour: number;
    public closeMinute: number;
    private bluetooth: Bluetooth;
    private peripheralId: string;

    // time that the motor is active to open and close the door
    public motorDurationSecs: number;

    // additional time for compensating motor moving against gravity in tens of a second
    // this time is only applied when motor is moving the door up
    public additionalTimeTens: number;

    // service ids
    private readonly SERVICE_UUID: string = '631be1aa-1e60-4bca-85cb-6c2768a49124';
    private readonly OPEN_CHARACTERISTIC_UUID = '14762daf-c9c6-40dd-8f3b-a3b9aa1fbbdf';
    private readonly CLOSE_CHARACTERISTIC_UUID = 'f305f47c-8aba-4228-93e0-b173b63a05c4';
    private readonly DIRECTUP_CHARACTERISTIC_UUID = 'ab19c32b-3c2c-4d96-bfb5-cf4eb8712c46';
    private readonly DIRECTDOWN_CHARACTERISTIC_UUID = '4d243c2a-b385-4c4c-8b85-d5b496f15b27';
    private readonly SETTINGS_CHARACTERISTIC_UUID = '4a0115c6-c88e-45ab-ae37-b16e61fb1603';

    constructor(bluetooth?: Bluetooth, peripheralId?: string) {
        super();
        this.openHour = 12;
        this.openMinute = 12;
        this.closeHour = 12;
        this.closeMinute = 12;
        bluetooth ? this.bluetooth = bluetooth : this.bluetooth = null;
        peripheralId ? this.peripheralId = peripheralId : this.peripheralId = null;
        this.readSettings();
        this.motorDurationSecs = 10;
        this.additionalTimeTens = 0;
    }

    async readSettings(): Promise<void> {

        // read open/close time settings
        let res = await this.bluetooth.read({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.OPEN_CHARACTERISTIC_UUID,
        });
        let bytes = new Uint8Array(res.value);
        let msg = await new TextDecoder('utf-8').decode(bytes);
        let msgTokens = msg.split(';');
        this.set('openHour', parseInt(msgTokens[0]));
        this.set('openMinute', parseInt(msgTokens[1]));
        res = await this.bluetooth.read({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.CLOSE_CHARACTERISTIC_UUID,
        });
        bytes = new Uint8Array(res.value);
        msg = await new TextDecoder('utf-8').decode(bytes);
        msgTokens = msg.split(';');
        this.set('closeHour', parseInt(msgTokens[0]));
        this.set('closeMinute', parseInt(msgTokens[1]));

        // read settings
        res = await this.bluetooth.read({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.SETTINGS_CHARACTERISTIC_UUID,
        });
        bytes = new Uint8Array(res.value);
        msg = await new TextDecoder('utf-8').decode(bytes);
        msgTokens = msg.split(';');
        this.set('motorDurationSecs', parseInt(msgTokens[0]));
        this.set('additionalTimeTens', parseInt(msgTokens[1]));
    }

    async storeOpenTime(): Promise<void> {
        const msg = `${this.openHour};${this.openMinute};0`;
        const bytes = await new TextEncoder('utf-8').encode(msg);
        this.bluetooth.write({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.OPEN_CHARACTERISTIC_UUID,
            value: bytes,
        });
    }

    async storeCloseTime(): Promise<void> {
        const msg = `${this.closeHour};${this.closeMinute};0`;
        const bytes = await new TextEncoder('utf-8').encode(msg);
        this.bluetooth.write({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.CLOSE_CHARACTERISTIC_UUID,
            value: bytes,
        });
    }

    onPickerLoaded(args: EventData): void {
        const timePicker = args.object as TimePicker;
        if (isAndroid) {
            timePicker.android.setIs24HourView(java.lang.Boolean.TRUE);
        }
    }

    async openDoor(): Promise<void> {
        Toast.makeText('open door').show();
        const bytes = new Uint8Array([1]);
        this.bluetooth.write({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.DIRECTUP_CHARACTERISTIC_UUID,
            value: bytes,
        });
    }

    async closeDoor(): Promise<void> {
        Toast.makeText('close door').show();
        const bytes = new Uint8Array([1]);
        this.bluetooth.write({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.DIRECTDOWN_CHARACTERISTIC_UUID,
            value: bytes,
        });
    }

    // msg format: <motor duration in secs>;<additional time in tens>
    async saveSettings(): Promise<void> {

        // check input, should be int from 1 to 255
        this.motorDurationSecs = Math.round(this.motorDurationSecs);
        if (this.motorDurationSecs < 1 || this.motorDurationSecs > 255) {
            Toast.makeText("Motor Einschaltzeit muss zwischen 1 und 255 Sekunden sein").show();
            return;
        }

        const msg = `${this.motorDurationSecs};${this.additionalTimeTens}`;
        const bytes = await new TextEncoder('utf-8').encode(msg);

        console.log(bytes);

        this.bluetooth.write({
            peripheralUUID: this.peripheralId,
            serviceUUID: this.SERVICE_UUID,
            characteristicUUID: this.SETTINGS_CHARACTERISTIC_UUID,
            value: bytes,
        });
    }
}
