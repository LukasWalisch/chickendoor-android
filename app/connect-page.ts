import { EventData } from '@nativescript/core/data/observable';
import { Page } from '@nativescript/core/ui/page/page';
import { ConnectPageModel } from './connect-page-model';

export function navigatingTo(args: EventData) {
    const page = args.object as Page;
    page.bindingContext = new ConnectPageModel();
}
