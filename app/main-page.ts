import { EventData } from '@nativescript/core/data/observable';
import { Page } from '@nativescript/core/ui/page/page';
import { HelloWorldModel } from './main-view-model';


// only for testing purposes, uncomment in production
export function navigatingTo(args: EventData) {
    const page = args.object as Page;
    page.bindingContext = new HelloWorldModel();
}
